#!/usr/bin/env python3

import os, sys
import subprocess as sp

def get_asgn():
    """
    Returns the name of the assignment directory that was last modified with git.
    For example, if asgn0/hello.c was modified, "asgn0" is returned.
    """
    cmd = 'git log --stat --oneline --name-only --format="" | egrep -o "asgn[0-9]+" | head -n 1 '
    asgn = sp.run(cmd, capture_output=True, shell=True).stdout.decode("utf-8", errors="ignore")
    return asgn.replace("\n", "")

def main():
    os.chdir("..")
    asgn = get_asgn()
    if not asgn or asgn.isspace():
        print("Latest git commit does not modify any of the assignments.")
        print("Exiting...")
        sys.exit(1)

    asgn = int(asgn[-1].strip(), 10)

    print(f"Entering asgn{asgn}")
    os.chdir(f"asgn{asgn}")

    if os.path.exists('Makefile'):
        status = os.system("make")
    else:
        print("No Makefile.")
        status = 1

    # Exit with grader status so job can be reported as failed.
    sys.exit(1 if status else 0)

if __name__ == "__main__":
    main()
